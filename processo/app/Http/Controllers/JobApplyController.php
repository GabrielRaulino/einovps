<?php

namespace App\Http\Controllers;

use App\Models\JobApply;
use App\Models\JobPosting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use App\Mail\SendEmail;

class JobApplyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param  String  $slug
     * @return \Illuminate\Http\Response
     */
    public function create(String $slug){


        $curriculo = filter_input(INPUT_GET, 'curriculum');
        $salario = filter_input(INPUT_GET, 'salary_claim');
        $desafio = filter_input(INPUT_GET, 'challenge_date');
        $userId = Auth::user()->id;

        $validator = Validator::make(compact('slug'),[
            'slug' => [
                'required',
                Rule::exists('job_postings')->where(function ($query) use ($slug) {
                    return $query->where('slug', $slug)->where('valid_through','>=',now());
                }),
            ],
        ],[
            'slug.exists' => 'Que pena, está vaga já não está mais disponível.',
        ]);

        if ($validator->fails()) {
            return redirect(route('jobposting.index'))
                ->withErrors($validator);
        }


        $jobposting = JobPosting::where('slug','=',$slug)->where('valid_through','>=',now())->first();
        $jobId = json_decode(JobPosting::select('job_posting_id')->where('slug', '=', $slug)->first(), true);


        foreach($jobId as $data){
            $jobId = $data;
        }
        if($curriculo){

            $candidatura = json_decode(JobApply::where('user-id', '=', $userId)->first());
            $job = json_decode(JobApply::where('job-posting-id', '=', $jobId)->first());

            if($candidatura && $job){
                $delete = JobApply::where('job-posting-id', '=', $jobId)
                ->delete();
            }
                JobApply::insert([
                    'curriculum' => $curriculo,
                    'salary-claim' => $salario,
                    'challenge-date' => $desafio,
                    'user-id' => $userId,
                    'job-posting-id' =>$jobId
                ]);

                // Mail::to(Auth::user()->email)->send(new SendEmail);

                return redirect('/mensagem');                     
        } 
        
        
        return view('jobapply.create',compact('jobposting'));

       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function show(JobApply $jobApply)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function edit(JobApply $jobApply)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JobApply $jobApply)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JobApply  $jobApply
     * @return \Illuminate\Http\Response
     */
    public function destroy(JobApply $jobApply)
    {
        //
    }
}
